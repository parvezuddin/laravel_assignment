@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12" style="background: #fff;">
			<div class="panel-heading">
				<h1>All Posts<a href="{{route('posts.create')}}" class="btn btn-primary pull-right">Create Post</a></h1>
			</div>
			@if(session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
			@endif
			@if(session('status1'))
			<div class="alert alert-danger">
				{{session('status1')}}
			</div>
			@endif
			
			<div class="col-sm-12 col-sm-offset-0">
				<div class="panel panel-primary">
					<div class="panel-heading">Post List</div>
					<div class="panel-body">
						<table id="myTable" class="table table-bordered table-striped">
						<thead>
							<tr>
								<td>Sl</td>
								<td>Title</td>
								<td>Description</td>
								<td>Image</td>
								<td>Created by</td>
								<td>Category</td>
								<td>Date</td>
								<td width="180">Action</td>
							</tr>
						</thead>
						<tbody>
							@foreach($posts as $post)
							<tr>
								<td>{{$post->id}}</td>
								<td>{{$post->title}}</td>
								<td>{{$post->description}}</td>
								<td><img src="{{$post->image}}" class="img-thumbnail" width="100" height="100"></td>
								<td>{{$post->user->name}}</td>
								<td>{{$post->post_category->name}}</td>
								<td>{{$post->user->created_at->toFormattedDateString()}}</td>
								<td>
									<a class="btn btn-primary btn-lg" href="{{route('posts.show',$post->id)}}"><i class="glyphicon glyphicon-eye-open"></i></a>
									<a class="btn btn-warning btn-lg" href="{{route('posts.edit',$post->id)}}"><i class="glyphicon glyphicon-edit"></i></a>
									<a href="#" onclick="return confirm('Are You Sure')" >
										<form style="display: inline-block;" action="{{route('posts.destroy',$post->id)}}" method="POST">
											{{csrf_field()}}
											{{method_field('DELETE')}}
											<button class="btn btn-danger btn-lg"><i class="glyphicon glyphicon-trash" value="Delete"></i></button>
										</form>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endsection