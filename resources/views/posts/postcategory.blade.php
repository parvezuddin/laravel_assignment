@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12" style="background: #fff;">
			<div class="panel-heading">
				<h1>All Categories <a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">Add a category</a></h1>
			</div>
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h4 class="modal-title" id="myModalLabel">Create Category</h4>
				      </div>
						    <div class="modal-body">
						        <form action="{{route('postcategorys.store')}}" method="POST">
						        	{{csrf_field()}}
						        	<div class="form-group">
						        		<label for="name">Post Category Name</label>
						        		<input type="text" name="name"  id="name" class="form-control">
						        	</div>
						        	<div class="form-group">
						        		<label for="slug">Post Category Slug</label>
						        		<input type="text" name="slug"  id="slug" class="form-control">
						        	</div>
						        	<div class="form-group">
						        		<label for="description">Post Category Description</label>
						        		<textarea name="description" id="description" cols="30" rows="5" class="form-control"></textarea>
						        	</div>
						        	<button type="submit" class="btn btn-primary">Add</button>
						        </form>
							</div>
					      	<div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      	</div> 
				    </div>
				  </div>
				</div>
				<div class="col-sm-12 col-sm-offset-0">
				<div class="post-categories">
					<div style="margin-bottom: 10px;" class="btn-group" rol="group" aria-label="...">
						@foreach($postcategorys as $postcategory)
						<a href="{{route('postcategorys.show', $postcategory->id)}}"><button type="button" class="btn btn-success">{{$postcategory->name}}</button></a>
						@endforeach
					</div>
				</div>
				</div>
				<div class="col-sm-12 col-sm-offset-0">
					<div class="panel panel-primary">
						<div class="panel-heading">Post Detials</div>
							<div class="panel-body">
						<table id="myTable" class="table table-bordered table-striped">
							<thead>
								<tr>
									<td>Sl</td>
									<td>Title</td>
									<td>Description</td>
									<td>Image</td>
									<td>Created By</td>
									<td>Date</td>
								</tr>
							</thead>
							<tbody>
								@if(isset($posts))
								@foreach($posts as $post)
									<tr>
										<td>{{$post->id}}</td>
										<td>{{$post->title}}</td>
										<td>{{$post->description}}</td>
										<td><img src="{{asset($post->image)}}" alt="" class="img-responsive" width="100" height="100"></td>
										<td>{{$post->user->name}}</td>
										<td>{{$post->created_at->toFormattedDateString()}}</td>
									</tr>
								@endforeach
								
								@endif
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endsection