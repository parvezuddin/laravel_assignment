@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-12" style="background: #fff;">
			<div class="panel-heading">
				<h1>View Post<a href="{{route('posts.index')}}" class="btn btn-success pull-right">Back</a></h1>
			</div>
			<div class="col-sm-12 col-sm-offset-0">
				<div class="panel panel-primary">
					<div class="panel-heading">Post Detials</div>
					<div class="panel-body">
				<h3>Post Title : {{$post->title}}</h3>
				<p>Description: {{$post->description}}</p>
				<p>Category: {{$post->post_category->name}}</p>
				<p>Image: <img src="{{asset($post->image)}}" width="80" height="80"></p>
				
			</div>
		</div>
	</div>
			</div>
		</div>
	</div>
@endsection