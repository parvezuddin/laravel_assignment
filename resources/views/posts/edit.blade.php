@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12" style="background: #fff;">
			<div class="panel-heading">
				<h1 style="margin-left:155px; ">Update Post<a href="{{route('posts.index')}}" class="btn btn-success pull-right">Back</a></h1>
			</div>
			@if(session('status'))
			<div class="alert alert-success">
				{{session('status')}}
			</div>
			@endif
			<div class="col-sm-8 col-sm-offset-2">
				@if(count($errors->all())>0)
				<div class="alert alert-danger">
					<ul>	
						@foreach($errors->all() as $error)
							<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<div class="panel panel-primary">
					<div class="panel-heading">Update Posts</div>
					<div class="panel-body">
						<form action="{{route('posts.update',$post->id)}}" method="POST" enctype="Multipart/form-data">
							{{method_field('PUT')}}
							{{csrf_field()}}
									<div class="form-group">
										<label for="title">Title</label>
										<input type="text" name="title" id="title" value="{{$post->title}}" class="form-control">
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="image">Image</label>
												<input type="file" name="image" id="image" class="form-control">
											</div>

										<div class="col-sm-6">
											<div class="form-group">
											<label for="post_category_id">Select a Category</label>
											<select name="post_category_id" id="post_category_id">
												@foreach($postcategorys as $postcategory)
												<option value="{{$postcategory->id}}" @if($postcategory->id == $post->post_category_id) Selected @endif>{{$postcategory->name}}</option>
												@endforeach
											</select>
											</div>
										</div>
										
										<div class="col-sm-6">
											<img class="img-thumbnail" src="{{asset($post->image)}}" width="80" height="80">
										</div>
										</div>
									</div>
										<div class="form-group">
											<label for="description">Description</label>
											<textarea name="description" id="description" cols="20" rows="5" class="form-control">{{$post->description}}</textarea>
										</div>

							<button class="btn btn-primary">Update</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection